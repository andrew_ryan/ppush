fn main(){
    // init_repository();
    doe::system("git pull").unwrap();
    git_add();
    commit_changes("ppush add pull_updates");
    doe::system("git push").unwrap();
}
use git2::{Repository, Signature};
use std::fs;

#[allow(warnings)]
fn init_repository() {
    // Create the .git directory
    fs::create_dir_all(".git").unwrap();

    // Initialize the Git repository
    let repo = Repository::init(".").unwrap();

    // Set up the initial commit
    let signature = Signature::now("Andrew_Ryan", "dnrops@outlook.com").unwrap();

    let tree_id = repo.index().unwrap().write_tree().unwrap();
    let tree = repo.find_tree(tree_id).unwrap();
    let root_commit = repo
        .commit(
            Some("HEAD"),
            &signature,
            &signature,
            "Initial commit",
            &tree,
            &[],
        )
        .unwrap();
    let mut config = repo.config().unwrap();
    config.set_str("init.defaultBranch", "master").unwrap();
    config.snapshot().unwrap();
    // Update 'HEAD' to point to the initial commit
    repo.reference(
        "refs/heads/master",
        root_commit,
        true,
        "initial",
    )
    .unwrap();

    println!("Initialized empty Git repository.");
}


#[allow(warnings)]
fn git_add() {
    // Open the repository
    let repo = Repository::open(".").unwrap();

    // Add all files in the repository to the index
    let mut index = repo.index().unwrap();
    index.add_all(["*"].iter(), git2::IndexAddOption::DEFAULT, None)
        .unwrap();
    index.write().unwrap();

    println!("Added all files to the Git index.");
}


#[allow(warnings)]
fn commit_changes(msg:impl ToString) {
    let msg  = msg.to_string();
    let repo = match Repository::open(".") {
        Ok(repo) => repo,
        Err(e) => {
            println!("Failed to open repository: {}", e);
            return;
        }
    };

    let signature = Signature::now("Andrew_Ryan", "dnrops@outlook.com").unwrap();

    match repo.index() {
        Ok(mut index) => {
            index.add_all(["*"].iter(), git2::IndexAddOption::DEFAULT, None)
                .unwrap();
            let oid = index.write_tree().unwrap();
            let tree = repo.find_tree(oid).unwrap();

            let parent_commit = match repo.head().unwrap().target() {
                Some(commit) => repo.find_commit(commit).unwrap(),
                None => {
                    println!("No commits found. Please make an initial commit.");
                    return;
                }
            };

            repo.commit(
                Some("HEAD"),
                &signature,
                &signature,
                &msg,
                &tree,
                &[&parent_commit],
            )
            .unwrap();

            println!("Changes committed successfully.");
        }
        Err(e) => {
            println!("Failed to get repository index: {}", e);
        }
    }
}
